package ru.itpark;

public class Main {
    public static void main(String[] args) {
        NewThread newThread1 = new NewThread("First Thread");
        NewThread newThread2 = new NewThread("Second Thread");
        NewThread newThread3 = new NewThread("Thrid Thread");

        System.out.println("Поток newThread1 запущет " +  newThread1.t.isAlive());
        System.out.println("Поток newThread2 запущет " +  newThread2.t.isAlive());
        System.out.println("Поток newThread3 запущет " +  newThread3.t.isAlive());

        //ожидать заверешение потоков

        try {
            System.out.println("Ожидание завершения потоков");
            newThread1.t.join();
            newThread2.t.join();
            newThread3.t.join();
        } catch (InterruptedException e){
            System.out.println("Главный поток прерван");
        }
        System.out.println("Поток один запущен" + newThread1.t.isAlive());
        System.out.println("Поток два запущен" + newThread2.t.isAlive());
        System.out.println("Поток три запущен" + newThread3.t.isAlive());


        System.out.println("Главный поток завершен");
    }
}
